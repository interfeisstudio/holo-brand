<?php
/*
Plugin Name: Holo Brand
Plugin URI: http://www.interfeis.com/
Description: Holo Brand is a wordpress plugin for display brand or logo.
Version: 1.1.2
Author: interfeis
Author URI: http://www.interfeis.com/
License: GPL
*/

/*  Copyright 2015 Interfeis

    Holo Brand is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//to block direct access
if ( ! defined( 'ABSPATH' ) )
	die( "Can't load this file directly" );

//global variable for this plugin
$pathinfo	= pathinfo(__FILE__);
$plugins_dir = dirname( __FILE__ );

include_once( $plugins_dir.'/widgets/holo-brand-widget.php' );
include_once( $plugins_dir.'/widgets/holo-brand-carousel-widget.php' );

class Holo_Brand{

	var $imagesizes;
	var	$langval;
	var	$version;
	var $defaultattr;
	var $postslug;
	var $taxonomslug;
	var $posttype;
	var $posttaxonomy;

	function __construct(){
		// Register the shortcode to the function ep_shortcode()
		add_shortcode( 'brand_carousel', array($this, 'holo_brandcarousel') );
		add_shortcode( 'brand', array($this, 'holo_brandcolumns') );

        add_action('vc_before_init', array( $this, 'holo_vc_map'));

		// Register the options menu
		add_action('admin_init', 'flush_rewrite_rules');

		//Register the Portfolio Menu
		add_action('init', array($this, 'holo_pf_post_type'));
		add_action('init', array($this, 'holo_pf_action_init'));
		add_action('after_setup_theme', array($this, 'holo_pf_setup'));

		//Customize the Portfolio List in the wp-admin
		add_filter('manage_edit-brand_columns', array($this, 'holo_pf_add_list_columns'));
		add_action('manage_brand_posts_custom_column', array($this, 'holo_pf_manage_column'));
		add_action( 'restrict_manage_posts', array($this, 'holo_pf_add_taxonomy_filter') );

		$this->version		= $this->holo_plugin_version();
		$this->postslug		= $this->holo_postslug();
		$this->taxonomslug	= $this->holo_taxonomslug();
		$this->posttype		= $this->holo_posttype();
		$this->posttaxonomy	= $this->holo_taxonomy();
	}

	//Get the version of portfolio
	function holo_plugin_version(){
		$this->version = "1.0";

		return $this->version;
	}

	function holo_lang(){
		$thelang = 'holo';
		return $thelang;
	}

	function holo_shortname(){
		$theshortname = 'holo';
		return $theshortname;
	}

	function holo_initial(){
		$theinitial = 'nvr';
		return $theinitial;
	}

	function holo_posttype(){
		$this->posttype = 'brand';
		return $this->posttype;
	}

	function holo_taxonomy(){
		$this->posttaxonomy = 'brandcat';
		return $this->posttaxonomy;
	}

	function holo_postslug(){
		$this->postslug = 'brand';
		return $this->postslug;
	}

	function holo_taxonomslug(){
		$this->taxonomslug = 'brandcat';
		return $this->taxonomslug;
	}

	function holo_pf_md5hash($str = ''){
		return md5($str);
	}

	//Get the image size for every column
	function holo_pf_setsize(){

		//set image size for every column in here.
		$this->imagesizes = array(
			array(
				"num"		=> 'default',
				"namesize"	=> 'brand-image',
				"width" 	=> 220,
				"height" 	=> 104
			)

		);
		return $this->imagesizes;
	}

	function holo_pf_setup(){
		add_theme_support( 'post-thumbnails' );
		$imagesizes = $this->holo_pf_setsize();
		foreach($imagesizes as $imgsize){
			add_image_size( $imgsize["namesize"], $imgsize["width"], $imgsize["height"], true ); // Portfolio Thumbnail
		}
	}

	function holo_pf_getthumbinfo($col){
		$imagesizes = $this->holo_pf_setsize();
		foreach($imagesizes as $imgsize){
			if($col==$imgsize["num"]){
				return $imgsize;
			}
		}
		return false;
	}

	function holo_brandcarousel($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => '',
					"cat" => '',
					"showposts" => '-1'
		), $atts));

			$nvr_output  ='<div class="brand '.esc_attr( $class ).'">';

			$i=1;
			$nvr_argquery = array(
				'post_type' => $this->holo_posttype(),
				'showposts' => $showposts
			);
			if($cat){
				$nvr_argquery['tax_query'] = array(
					array(
						'taxonomy' => $this->holo_taxonomy(),
						'field' => 'slug',
						'terms' => $cat
					)
				);
			}

			$nvr_brandqry = new WP_Query( $nvr_argquery );

			$nvr_output  .='<div class="flexslider-carousel row">';
				$nvr_output  .='<ul class="slides">';

				$nvr_havepost = false;
				if( $nvr_brandqry->have_posts() ){
					while ($nvr_brandqry->have_posts()) : $nvr_brandqry->the_post();
						$nvr_havepost = true;
						$excerpt = get_the_excerpt();
						$postid = get_the_ID();
						$custom = get_post_custom( $postid );
                        $imgsize = 'brand-img';
						$cthumb = (isset($custom["carousel_thumb"][0]))? $custom["carousel_thumb"][0] : "";
						$extlink = (isset($custom["external_link"][0]))? $custom["external_link"][0] : "";

						$thumbid = get_post_thumbnail_id( $postid );
						$alttext = get_post_meta($postid, '_wp_attachment_image_alt', true);
						$imagesrc = wp_get_attachment_image_src( $thumbid, 'brand-image' );

						if($cthumb!=""){
							$imagethumb = $cthumb;
							$alttext = get_the_title( $postid );
						}else{
							if($imagesrc!=false){
								$imagethumb = $imagesrc[0];
							}else{
								$imagethumb = plugin_dir_url( __FILE__ ).'images/noimage.png';
								$alttext = get_the_title( $postid );
							}
						}

						$nvr_output  .='<li>';
							$nvr_output .= '<div class="cr-item-container">';
								if($extlink){
									$nvr_output  .='<a href="'.esc_url( $extlink ).'" target="_blank"><img src="'.esc_url( $imagethumb ).'" alt="'.esc_attr( $alttext ).'" /></a>';
								}elseif( has_post_thumbnail( $postid ) ){
									$nvr_output .= get_the_post_thumbnail( $postid, $imgsize, array('class' => 'scale-with-grid'));
								}else{
									$nvr_output  .='<img src="'.esc_url( $imagethumb ).'" alt="'.esc_attr( $alttext ).'" />';
								}
							$nvr_output .= '</div>';
						$nvr_output  .='</li>';

						$i++; $addclass="";
					endwhile;
				}
				wp_reset_postdata();

				$nvr_output .='</ul>';
			 $nvr_output .='</div>';
			 $nvr_output .='</div>';
			 if($nvr_havepost){
			 	return do_shortcode($nvr_output);
			}else{
				return false;
			}
	}

	function holo_brandcolumns($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => '',
					"cat" => '',
					"col" => 4,
					"showposts" => '-1'
		), $atts));

			$i=1;
			$nvr_argquery = array(
				'post_type' => $this->holo_posttype(),
				'showposts' => $showposts
			);
			if($cat){
				$nvr_argquery['tax_query'] = array(
					array(
						'taxonomy' => $this->holo_taxonomy(),
						'field' => 'slug',
						'terms' => $cat
					)
				);
			}

			$nvr_brandqry = new WP_Query( $nvr_argquery );

			$column = intval($col);

			if($column!= 2 && $column!= 3 && $column!= 4 ){
				$column = 3;
			}
			$nvr_output = '';

			$nvr_output  .='<div class="brand-container '.esc_attr( $class ).'">';
				$nvr_output  .='<div class="row brand-row">';

				$nvr_havepost = false;

				if( $nvr_brandqry->have_posts() ){
					while ($nvr_brandqry->have_posts()) : $nvr_brandqry->the_post();
						$nvr_havepost = true;
						$excerpt = get_the_excerpt();
						$postid = get_the_ID();
                        $imgsize = 'brand-img';
						$custom = get_post_custom( $postid );
						$cthumb = (isset($custom["carousel_thumb"][0]))? $custom["carousel_thumb"][0] : "";
						$extlink = (isset($custom["external_link"][0]))? $custom["external_link"][0] : "";

						$thumbid = get_post_thumbnail_id( $postid );
						$alttext = get_post_meta($postid, '_wp_attachment_image_alt', true);
						$imagesrc = wp_get_attachment_image_src( $thumbid, 'brand-image' );

						if($cthumb!=""){
							$imagethumb = $cthumb;
							$alttext = get_the_title( $postid );
						}else{
							if($imagesrc!=false){
								$imagethumb = $imagesrc[0];
							}else{
								$imagethumb = plugin_dir_url( __FILE__ ).'images/noimage.png';
								$alttext = get_the_title( $postid );
							}
						}

						if($column=="2"){
							$classbr = 'six columns ';
						}elseif($column=="4"){
							$classbr = 'three columns ';
						}else{
							$classbr = 'four columns ';
						}

						if(($i%$column) == 1){ $classbr .= "first ";}
						if(($i%$column) == 0){$classbr .= "last ";}

						if(($i%$column) == 1 && $i>1){
							$nvr_output  .='</div>';
							$nvr_output  .='<div class="row brand-row">';
						}

						$nvr_output  .='<div class="'.esc_attr( $classbr ).'">';
							$nvr_output .= '<div class="br-item-container">';
								if($extlink){
									$nvr_output  .='<a href="'.esc_url( $extlink ).'" target="_blank"><img src="'.esc_url( $imagethumb ).'" alt="'.esc_attr( $alttext ).'" /></a>';
								}elseif( has_post_thumbnail( $postid ) ){
									$nvr_output .= get_the_post_thumbnail( $postid, $imgsize, array('class' => 'scale-with-grid'));
								}else{
									$nvr_output  .='<img src="'.esc_url( $imagethumb ).'" alt="'.esc_attr( $alttext ).'" />';
								}
							$nvr_output .= '</div>';
						$nvr_output  .='</div>';

						$i++; $addclass="";
					endwhile;
				}
				wp_reset_postdata();

			 	$nvr_output .='</div>';
			 $nvr_output .='</div>';

			 if($nvr_havepost){
			 	return do_shortcode($nvr_output);
			}else{
				return false;
			}
	}

	/* Make a Portfolio Post Type */
	function holo_pf_post_type() {
		$posttype = $this->holo_posttype();
		$taxonom = $this->holo_taxonomy();
		$postslug = $this->holo_postslug();
		$taxonomslug = $this->holo_taxonomslug();

		register_post_type( $posttype,
					array(
					'label' => __('Brand', 'holo-brand' ),
					'public' => true,
					'show_ui' => true,
					'show_in_nav_menus' => true,
					'rewrite' => array( 'slug' => $postslug, 'with_front' => false ),
					'hierarchical' => true,
					'menu_position' => 5,
					'has_archive' => true,
					'supports' => array(
										 'title',
										 'editor',
										 'thumbnail',
										 'excerpt',
										 'custom-fields',
										 'revisions')
						)
					);
		register_taxonomy($taxonom, $posttype, array(
			'hierarchical' => true,
			'label' =>  __('Brand Categories', 'holo-brand'),
			'query_var' => true,
			'rewrite' => array( 'slug' => $taxonomslug, 'with_front' => false ),
			'show_ui' => true,
			'singular_name' => 'Category'
			));
	}

	function holo_pf_add_list_columns($portfolio_columns){

		$thetaxonomy = $this->holo_taxonomy();
		$new_columns = array();
		$new_columns['cb'] = '<input type="checkbox" />';

		$new_columns['title'] = __('Brand Title', 'holo-brand');
		$new_columns['images'] = __('Images', 'holo-brand');
		$new_columns['author'] = __('Author', 'holo-brand');

		$new_columns[$thetaxonomy] = __('Categories', 'holo-brand');

		$new_columns['date'] = __('Date', 'holo-brand');

		return $new_columns;
	}

	function holo_pf_manage_column($column_name){
		global $post;
		$posttype = $this->holo_posttype();
		$taxonom = $this->holo_taxonomy();

		$id = $post->ID;
		$title = $post->post_title;
		switch($column_name){
			case 'images':
				$thumbnailid = get_post_thumbnail_id($id);
				$imagesrc = wp_get_attachment_image_src($thumbnailid, 'thumbnail');
				if($imagesrc){
					echo '<img src="'.$imagesrc[0].'" width="50" alt="'.$title.'" />';
				}else{
					_e('No Featured Image', 'holo-brand');
				}
				break;

			case $taxonom:
				$postterms = get_the_terms($id, $taxonom);
				if($postterms){
					$termlists = array();
					foreach($postterms as $postterm){
						$termlists[] = '<a href="'.admin_url('edit.php?'.$taxonom.'='.$postterm->slug.'&post_type='.$posttype).'">'.$postterm->name.'</a>';
					}
					if(count($termlists)>0){
						$termtext = implode(", ",$termlists);
						echo $termtext;
					}
				}

				break;
		}
	}

	/* Filter Custom Post Type Categories */
	function holo_pf_add_taxonomy_filter() {
		global $typenow;
		$posttype = $this->holo_posttype();
		$taxonomy = $this->holo_taxonomy();
		if( $typenow==$posttype){
			$filters = array($taxonomy);
			foreach ($filters as $tax_slug) {
				$tax_obj = get_taxonomy($tax_slug);
				$tax_name = $tax_obj->labels->name;
				$terms = get_terms($tax_slug);
				echo '<select name="'. esc_attr( $tax_slug ).'" id="'. esc_attr( $tax_slug ) .'" class="postform">';
				echo "<option value=''>".__('View All','holo-brand')." "."$tax_name</option>";
				foreach ($terms as $term) {
					$selectedstr = '';
					if(isset($_GET[$tax_slug]) && $_GET[$tax_slug] == $term->slug){
						$selectedstr = ' selected="selected"';
					}
					echo '<option value='. $term->slug. $selectedstr . '>' . $term->name .' (' . $term->count .')</option>';
				}
				echo "</select>";
			}
		}
	}

	function holo_pf_action_init(){
		// only hook up these filters if we're in the admin panel, and the current user has permission
		// to edit posts and pages

		$version = $this->holo_plugin_version();

		wp_register_script('flexslider', plugin_dir_url( __FILE__ ).'js/jquery.flexslider-min.js', array('jquery'), '1.8', true);
		wp_enqueue_script('flexslider');

		wp_register_script('holo_customBrand', plugin_dir_url( __FILE__ ).'js/holobrand.js', array('jquery'), '1.0', true);
		wp_enqueue_script('holo_customBrand');

		$nvr_localvar = array(
			'pluginurl'					=> plugin_dir_url( __FILE__ )
		);
		wp_localize_script( 'nvr_customBrand', 'nvrbrandlocal_var', $nvr_localvar );

		//Register and use this plugin main CSS
		wp_register_style('holo_skeleton', plugin_dir_url( __FILE__ ).'css/1140.css', 'normalize', '', 'screen, all');
		wp_enqueue_style('holo_skeleton');

		wp_register_style('flexslider', plugin_dir_url( __FILE__ ).'css/flexslider.css', '', '', 'screen, all');
		wp_enqueue_style('flexslider');

		wp_register_style('holo_custom-brand', plugin_dir_url( __FILE__ ).'css/holobrand.css', '', '', 'screen, all');
		wp_enqueue_style('holo_custom-brand');
	}

	// The excerpt based on character
	function holo_pf_limit_char($excerpt, $substr=0, $strmore = "..."){
		$string = strip_tags(str_replace('...', '...', $excerpt));
		if ($substr>0) {
			$string = substr($string, 0, $substr);
		}
		if(strlen($excerpt)>=$substr){
			$string .= $strmore;
		}
		return $string;
	}

    function holo_vc_map(){
        if(function_exists('vc_map')){
            vc_map(
                array(
                    "name" => __( "Brand Carousel", 'holo-brand'),
                    "base" => "brand_carousel",
                    "class" => "",
                    "category" => __( "Holo", 'holo-brand'),
                    "params" => array(
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __( "Custom Class", 'holo-brand' ),
                            "param_name" => "class",
                            "value" => "",
                            "description" => __( "Input your custom class. (optional)", 'holo-brand' )
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __( "Brand Category Slug", 'holo-brand' ),
                            "param_name" => "cat",
                            "admin_label" => true,
                            "value" => "",
                            "description" => __( "Input the brand category slugs.", 'holo-brand' )
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __( "Show Posts", 'holo-brand' ),
                            "param_name" => "showposts",
                            "value" => '-1',
                            "description" => __( "Input the number of brand that you want to display.", 'holo-brand' )
                        )
                    )
                )
            );

            vc_map(
                array(
                    "name" => __( "Brand", 'holo-brand'),
                    "base" => "brand",
                    "class" => "",
                    "category" => __( "Holo", 'holo-brand'),
                    "params" => array(
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __( "Custom Class", 'holo-brand' ),
                            "param_name" => "class",
                            "value" => "",
                            "description" => __( "Input your custom class. (optional)", 'holo-brand' )
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __( "Brand Category Slug", 'holo-brand' ),
                            "param_name" => "cat",
                            "admin_label" => true,
                            "value" => "",
                            "description" => __( "Input the brand category slugs.", 'holo-brand' )
                        ),
                        array(
                            "type" => "dropdown",
                            "class" => "",
                            "heading" => __( "Columns", 'holo-brand' ),
                            "param_name" => "col",
                            "admin_label" => true,
                            "value" => array(
                                __('2 Columns', 'holo-brand') => "2",
                                __('3 Columns', 'holo-brand') => "3",
                                __('4 Columns', 'holo-brand') => "4"
                            ),
                            "description" => __( "Select the column of your brand.", 'holo-brand' )
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __( "Show Posts", 'holo-brand' ),
                            "param_name" => "showposts",
                            "value" => '-1',
                            "description" => __( "Input the number of brand that you want to display.", 'holo-brand' )
                        )
                    )
                )
            );
        }
    }

}

$thebrand = new Holo_Brand();


add_action("widgets_init", "holo_brand_load_widgets");
function holo_brand_load_widgets() {
	register_widget("Holo_BrandCarouselWidget");
	register_widget("Holo_BrandWidget");
}
?>
