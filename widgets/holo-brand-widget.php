<?php
// =============================== Holo Brand Widget ======================================
class Holo_BrandWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_holo_brand', 'description' => esc_html__('Holo - Brand', "holo-brand") );
		parent::__construct('holo-brand-widget', esc_html__('Holo - Brand',"holo-brand"), $widget_ops);
	}
	
	function widget( $args, $instance ) {
		global $wpdb, $comments, $comment;

		extract($args, EXTR_SKIP);
		$class      = apply_filters('widget_holo_brand_class', empty($instance['class']) ? '' : $instance['class']);
        $cat        = apply_filters('widget_holo_brand_cat', empty($instance['cat']) ? '' : $instance['cat']);
        $col        = apply_filters('widget_holo_brand_col', empty($instance['col']) ? '' : $instance['col']);
        $showposts  = apply_filters('widget_holo_brand_showposts', empty($instance['showposts']) ? '' : $instance['showposts']);
		
        $scparams = '';
        if(trim($class)!=''){
            $scparams .= ' class="'.esc_attr($class).'"';
        }
        
        if(trim($cat)!=''){
            $scparams .= ' cat="'.esc_attr($cat).'"';
        }
        
        if(trim($col)!=''){
            $scparams .= ' col="'.esc_attr($col).'"';
        }
        
        if(trim($showposts)!=''){
            $scparams .= ' showposts="'.esc_attr($showposts).'"';
        }
        
        echo do_shortcode('[brand '.$scparams.']');
	}
	
	function update($new_instance, $old_instance) {				
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
		$instance['class'] = (isset($instance['class']))? $instance['class'] : "";
		$instance['cat'] = (isset($instance['cat']))? $instance['cat'] : "";
        $instance['col'] = (isset($instance['col']))? $instance['col'] : "";
        $instance['showposts'] = (isset($instance['showposts']))? $instance['showposts'] : "";
		
        $cols = array(
            '2' => __('2 Columns', 'holo-brand'),
            '3' => __('3 Columns', 'holo-brand'),
            '4' => __('4 Columns', 'holo-brand')
        );
        
        $class = esc_attr($instance['class']);
		$cat = esc_attr($instance['cat']);
        $col = esc_attr($instance['col']);
		$showposts = esc_attr($instance['showposts']);
        

        ?>
            <p><label for="<?php echo esc_attr( $this->get_field_id('class') ); ?>"><?php esc_html_e('Custom Class:', "holo-brand"); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('class') ); ?>" name="<?php echo esc_attr( $this->get_field_name('class') ); ?>" type="text" value="<?php echo esc_attr( $class ); ?>" /></label></p>
			
            <p><label for="<?php echo esc_attr( $this->get_field_id('cat') ); ?>"><?php esc_html_e('Brand Category Slug:', "holo-brand" ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('cat') ); ?>" name="<?php echo esc_attr( $this->get_field_name('cat') ); ?>" type="text" value="<?php echo esc_attr( $cat ); ?>" /></label></p>
            
            <p><label for="<?php echo esc_attr( $this->get_field_id('col') ); ?>"><?php esc_html_e('Number of Columns:', "holo-brand" ); ?> 
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('col') ); ?>" name="<?php echo esc_attr( $this->get_field_name('col') ); ?>">
                    <?php foreach($cols as $colval => $colname){ ?>
                        <?php $selected = ($colval==$col)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $colval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $colname ); ?></option>
                    <?php }?>
                </select>
            </label></p>
            
            <p><label for="<?php echo esc_attr( $this->get_field_id('showposts') ); ?>"><?php esc_html_e('Showposts:', "holo-brand" ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('showposts') ); ?>" name="<?php echo esc_attr( $this->get_field_name('showposts') ); ?>" type="text" value="<?php echo esc_attr( $showposts ); ?>" /></label></p>
        <?php 
    }
}
?>